/*
 * ScanFile.cpp
 *
 *  Created on: 2018年1月16日
 *      Author: coco
 */
#include <iostream>
#include <map>
#include <fstream>
#include <time.h>
#include <cstring>
#include <string>
#include <io.h>
#include <vector>
#include <stdlib.h>
#include "Smtp.h"

using namespace std;

vector<_finddata_t> getAllFiles(const char * dir);	//获取指定目录下的所有文件列表
void delay_sec(int sec);							//延迟函数，单位是秒
map<int, string> filesRead(string filename, unsigned long filesize);//读取制定文件内容，返回一个map结构key为数字，value为字符串
map<int, string> filesRead2(string filename);
string filesRead3(string filename);
void dispFileContent(map<int, string> var);			//在控制台显示文件内容
void dispFileContent(string filename);				//在控制台显示文件内容

int sendMail(string fileName, string mailSubject, string mailContent);

int main() {
	char dir[300];
	vector<_finddata_t> init_allfiles, tmp_allfiles;
	map<string, map<int, string> > init_mm, now_mm;
	map<int, string> m2;

	cout << "Enter a directory (ends with \'\\\'): ";
	cin.getline(dir, 300);
	string path = dir;  							//为下面打开文件读取内容增加路径
	strcat_s(dir, "*.*");        						//在要遍历的目录后加上通配符

	init_allfiles = getAllFiles(dir);
	int len = init_allfiles.size();

	for (int i = 0; i < len; i++) {
		m2 = filesRead(path + init_allfiles[i].name, init_allfiles[i].size);
		init_mm.insert(
				map<string, map<int, string> >::value_type(
						init_allfiles[i].name, m2));
	}

	while (1) {
		tmp_allfiles = getAllFiles(dir);
		len = tmp_allfiles.size();
		for (int i = 0; i < len; i++) {
			m2 = init_mm[tmp_allfiles[i].name];
			if (!m2.empty()) {									//判断取出的map是否为空
				unsigned long ul = strtoul(m2[0].c_str(), NULL, 10);//将map中记录的文件size转化为unsigned long进行对比
				if (ul != tmp_allfiles[i].size) {
					cout << tmp_allfiles[i].name << " file changed! Content is："
							<< endl;
					dispFileContent(path + tmp_allfiles[i].name);
					string emailContent = filesRead3(
							path + tmp_allfiles[i].name);
					char mailSubject[100] = "Changed account :";
					strcat_s(mailSubject,tmp_allfiles[i].name);
					int err = sendMail("email.ini", mailSubject,
							emailContent);
					if (err != 0) {
						if (err == 1)
							cout << "错误1: 由于网络不畅通，发送失败!" << endl;
						if (err == 2)
							cout << "错误2: 用户名错误,请核对!" << endl;
						if (err == 3)
							cout << "错误3: 用户密码错误，请核对!" << endl;
						if (err == 4)
							cout << "错误4: 请检查附件目录是否正确，以及文件是否存在!" << endl;
					}
					init_mm.erase(tmp_allfiles[i].name);
					init_mm.insert(
							map<string, map<int, string> >::value_type(
									tmp_allfiles[i].name,
									filesRead(path + tmp_allfiles[i].name,
											tmp_allfiles[i].size)));
				}
			}
		}
		delay_sec(300);
	}

	return 0;
}

/*
 * 延迟函数，参数为秒
 */
void delay_sec(int sec) {
	time_t start_time, cur_time;
	time(&start_time);
	do {
		time(&cur_time);
	} while ((cur_time - start_time) < sec);
}

/*
 * 遍历目录下的文件
 */
vector<_finddata_t> getAllFiles(const char * dir) {
	intptr_t handle;
	vector<_finddata_t> temp;

	_finddata_t findData;

	handle = _findfirst(dir, &findData);    // 查找目录中的第一个文件
	if (handle == -1) {
		cout << "Failed to find first file!\n";
		return temp;
	}

	do {
		if (findData.attrib & _A_SUBDIR) {
			continue;
		} else {
			temp.push_back(findData);
		}
	} while (_findnext(handle, &findData) == 0);    // 查找目录中的下一个文件
	_findclose(handle);    // 关闭搜索句柄
	return temp;
}

/*
 * 读取文件内容
 * 因为采用文件大小进行判断是否有变动，所以在从_finddata_t结构转为map的时候，把每个文件自身的大小放在第一个index中
 * 文件大小的数据类型为unsigned long类型，所以在保存为string的时候进行了转化
 * 在取出使用对比的时候需要再次进行转换为unsigned long
 */
map<int, string> filesRead(string filename, unsigned long filesize) {
	ifstream infile;
	map<int, string> filecontent;

	//检查文件是否打开成功
	infile.open(filename.c_str());
	if (!infile.is_open()) {
		cout << "Open file failed!" << endl;
	} else {
		string s;
		int i = 1;
		filecontent.insert(
				map<int, string>::value_type(0, to_string(filesize)));
		while (!infile.eof()) {
			getline(infile, s);
			if (!s.empty()) {
				filecontent.insert(map<int, string>::value_type(i, s));
				i++;
			}
		}
	}
	infile.close();
	return filecontent;
}

/*
 * 显示文件内容
 */
void dispFileContent(map<int, string> var) {
	map<int, string>::iterator it;
	for (it = var.begin(); it != var.end(); ++it) {
		cout << it->second << endl;
	}
}

void dispFileContent(string filename) {
	ifstream infile;
	//检查文件是否打开成功
	infile.open(filename.c_str());
	if (!infile.is_open()) {
		cout << "Open file failed!" << endl;
	} else {
		string s;
		int i = 0;
		while (!infile.eof()) {
			getline(infile, s);
			if (!s.empty()) {
				cout << s << endl;
				i++;
			}
		}
	}
	infile.close();
}

map<int, string> filesRead2(string filename) {
	ifstream infile;
	map<int, string> filecontent;

	//检查文件是否打开成功
	infile.open(filename.c_str());
	if (!infile.is_open()) {
		cout << "Open file failed!" << endl;
	} else {
		string s;
		int i = 0;
		while (!infile.eof()) {
			getline(infile, s);
			if (!s.empty()) {
				filecontent.insert(map<int, string>::value_type(i, s));
				i++;
			}
		}
	}
	infile.close();
	return filecontent;
}

string filesRead3(string filename) {
	ifstream infile;
	string str;
	//检查文件是否打开成功
	infile.open(filename.c_str());
	if (!infile.is_open()) {
		cout << "Open file failed!" << endl;
	} else {
		string s;
		int i = 0;
		while (!infile.eof()) {
			getline(infile, s);
			if (!s.empty()) {
				str = str + s + "\n";
				i++;
			}
		}
	}
	infile.close();
	return str;
}

int sendMail(string fileName, string mailSubject, string mailContent) {
	ifstream infile;
	string str;
	int err;

	CSmtp smtp(25, /*smtp端口*/
	"smtp.163.com", /*smtp服务器地址*/
	"xxxx@163.com", /*你的邮箱地址*/
	"xxxx", /*邮箱密码*/
	"xxxx@163.com", /*目的邮箱地址*/
	mailSubject, /*主题*/
	mailContent /*邮件正文*/
	);
	if (!smtp.SendEmail_Ex()){
		//检查文件是否打开成功
		infile.open(fileName.c_str());
		if (!infile.is_open()) {
			cout << "Open file failed!" << endl;
		}
		else {
			string s;
			int i = 0;			
			while (!infile.eof()) {
				getline(infile, s);
				if (!s.empty()) {
					smtp.SetTargetEmail(s);
					err = smtp.SendEmail_Ex();
					i++;
				}
			}
		}
		infile.close();
	}
	return err;
}
